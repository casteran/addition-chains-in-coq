 A study of various exponentiation algorithms based on euclidean addition chains.
 
  **keywords**: Coq proof assistant, reflexion, parametricity, addition chains, algorithmic.
  
  **compilation** Just type "make" in the top directory (works on Coq V8.12.0 + coq-paramcoq plug-in + lualatex)

  
  **Comments and contributions are welcome !**

  **Contact:** pierre dot casteran at [gmail dot com | labri dot fr]
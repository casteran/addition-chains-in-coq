all:    project pdf vo doc


pdf:
	(cd latex; make)

vo:
	(cd src; make -f CoqMakefile all)

clean:
	(cd src ; make -f CoqMakefile clean)

project: 
	(cd src ; source make_project)

doc:	html pdf

html:
	(cd src ; mkdir -p html; coqdoc -R . addition -g -d html -utf8 -toc */*.v)


